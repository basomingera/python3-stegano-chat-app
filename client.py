#!/usr/bin/python3
from __future__ import absolute_import, unicode_literals
from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread
import Tkinter as tk
import hashlib
import random
from PIL import ImageTk, Image
from os import listdir
from os.path import isfile, join
from steganography.steganography import Steganography


received_imgs_counter = 1
received_imgs_basename = "./receivedImages/image%s.png"
received_imgs_dict = {}
hiden_msg_counter = 1


root = tk.Tk()

NAMES = tk.StringVar()
USERNAME = tk.StringVar()
PASSWORD = tk.StringVar()

HOST = "202.30.29.243"
PORT = 12345
BUFSIZ = 256
client_socket = socket(AF_INET, SOCK_STREAM)
receive_thread = ''

my_msg = tk.StringVar()  # For the messages to be sent.
txt_hidden_msg = tk.StringVar()


def InitConnection():
    ADDR = (HOST, PORT)
    client_socket.connect(ADDR)


def init(the_root):
    scrollbar = tk.Scrollbar(root)
    scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
    width = 800
    height = 760
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    x = (screen_width / 2) - (width / 2)
    y = (screen_height / 2) - (height / 2)
    the_root.geometry("%dx%d+%d+%d" % (width, height, x, y))
    root.resizable(0, 0)


def WelcomePage():
    root.title("Infosec project: Welcome page")
    init(root)

    # ==============================FRAMES=========================================
    Top = tk.Frame(root, bd=2, relief=tk.RIDGE)
    Top.pack(side=tk.TOP, fill=tk.X)
    Form = tk.Frame(root, height=200)
    Form.pack(side=tk.TOP, pady=20)
    # ==============================LABELS=========================================
    lbl_welcome_title = tk.Label(Top, text="Welcome!", font=('arial', 15))
    lbl_welcome_title.pack(fill=tk.X)
    lbl_title = tk.Label(Top, text="Please Login or Register if it is the first time", font=('arial', 15))
    lbl_title.pack(fill=tk.X)
    lbl_username = tk.Label(Form, text="Username:", font=('arial', 14), bd=15)
    lbl_username.grid(row=0, sticky=tk.W)
    lbl_password = tk.Label(Form, text="Password:", font=('arial', 14), bd=15)
    lbl_password.grid(row=1, sticky="e")
    global lbl_text
    lbl_text = tk.Label(Form, text="", font=('arial', 14), bd=15)
    lbl_text.grid(row=2, columnspan=2)

    # ==============================ENTRY WIDGETS==================================
    username = tk.Entry(Form, textvariable=USERNAME, font=(14))
    username.grid(row=0, column=1)
    password = tk.Entry(Form, textvariable=PASSWORD, show="*", font=(14))
    password.grid(row=1, column=1)

    # ==============================BUTTON WIDGETS=================================
    btn_login = tk.Button(Form, text="Login", width=45, command=Login)
    btn_login.grid(pady=25, row=3, columnspan=2)
    btn_login.bind('<Return>', Login)

    btn_register = tk.Button(Form, text="Register", width=45, command=RegisterPage)
    btn_register.grid(pady=25, row=4, columnspan=2)
    btn_register.bind('<Return>', RegisterPage)


def RegisterPage():
    global RegisterWindow
    root.withdraw()
    RegisterWindow = tk.Toplevel()
    RegisterWindow.title("Registration page")
    init(RegisterWindow)

    # ==============================FRAMES=========================================
    Top = tk.Frame(RegisterWindow, bd=2, relief=tk.RIDGE)
    Top.pack(side=tk.TOP, fill=tk.X)
    reg_Form = tk.Frame(RegisterWindow, height=200)
    reg_Form.pack(side=tk.TOP, pady=20)
    # ==============================LABELS=========================================
    lbl_title = tk.Label(Top, text="Registration Page", font=('arial', 15))
    lbl_title.pack(fill=tk.X)

    lbl_names = tk.Label(reg_Form, text="Full name:", font=('arial', 14), bd=15)
    lbl_names.grid(row=0, sticky=tk.W)

    lbl_username = tk.Label(reg_Form, text="Username:", font=('arial', 14), bd=15)
    lbl_username.grid(row=1, sticky=tk.W)

    lbl_password = tk.Label(reg_Form, text="Password:", font=('arial', 14), bd=15)
    lbl_password.grid(row=2, sticky=tk.W)

    global reg_lbl_text
    reg_lbl_text = tk.Label(reg_Form, text="", font=('arial', 14), bd=15)
    reg_lbl_text.grid(row=3, columnspan=2)

    # ==============================ENTRY WIDGETS==================================
    names = tk.Entry(reg_Form, textvariable=NAMES, font=(14))
    names.grid(row=0, column=1)
    username = tk.Entry(reg_Form, textvariable=USERNAME, font=(14))
    username.grid(row=1, column=1)
    password = tk.Entry(reg_Form, textvariable=PASSWORD, show="*", font=(14))
    password.grid(row=2, column=1)

    # ==============================BUTTON WIDGETS=================================
    btn_register = tk.Button(reg_Form, text="Register", width=45, command=Register)
    btn_register.grid(pady=25, row=4, columnspan=2)
    btn_register.bind('<Return>', Register)


def Login():
    # HomePage()
    if USERNAME.get() == "" or PASSWORD.get() == "":
        lbl_text.config(text="Please complete the required field!", fg="red")
    else:
        MY_USERNAME = USERNAME.get()
        my_password = PASSWORD.get().encode('utf8')
        my_hashed_password = hashlib.sha224(my_password).hexdigest()
        PASSWORD.set("")
        lbl_text.config(text="logging in ..... ", fg="blue")
        login_msg = "log##"+str(MY_USERNAME)+"##"+str(my_hashed_password)
        client_socket.send(str(login_msg).encode("utf-8"))
        login_response = client_socket.recv(BUFSIZ).decode("utf8")
        if int(login_response) == 200:
            HomePage(MY_USERNAME)
        else:
            lbl_text.config(text="Invalid username or password", fg="red")
            USERNAME.set("")


def Register():
    print("registering")
    if USERNAME.get() == "" or PASSWORD.get() == "":
        reg_lbl_text.config(text="Please complete the required field!", fg="red")
    else:
        MY_USERNAME = USERNAME.get()
        my_password = PASSWORD.get().encode('utf8')
        my_hashed_password = hashlib.sha224(my_password).hexdigest()
        reg_lbl_text.config(text="Registering ..... ", fg="blue")
        reg_msg = "reg##" + str(MY_USERNAME) + "##" + str(my_hashed_password)
        client_socket.send(reg_msg.encode("utf-8"))
        reg_response = client_socket.recv(BUFSIZ).decode("utf8")
        if int(reg_response) == 500:
            reg_lbl_text.config(text=" Username taken", fg="red")
        elif int(reg_response) == 200:
            HomePage(MY_USERNAME)
        else:
            reg_lbl_text.config(text="Error", fg="red")
            USERNAME.set("")
            PASSWORD.set("")


def receive():
    """Handles receiving of messages."""
    while True:
        try:
            msg = client_socket.recv(BUFSIZ)
            print("received msg: ", msg)
            if 'SIZE OF IMAGE RECEIVED' in msg.decode("utf8"):
                print("sending size of image: ", msg)
                sendImage()
                print("image sent to server")

            elif 'IMAGE SIZE IS#' in msg.decode("utf8"):
                print("received image size ", msg.decode("utf8"))
                rec_image_size = msg.decode().split('#')[1]
                rec_image_sender = msg.decode().split('#')[-1]
                got_image_size_mgs = 'SIZE OF IMAGE RECEIVED:%s' % rec_image_size

                client_socket.send(got_image_size_mgs.strip().encode("utf-8"))
                print("sent img size ack ")

                image_data = client_socket.recv(int(rec_image_size))
                print("received image data of size %s" % str(len(image_data)))

                global received_imgs_counter
                global received_imgs_basename
                print("counter", received_imgs_counter)
                image_file = open(received_imgs_basename % received_imgs_counter, "wb")
                image_file.write(image_data)
                image_file.close()

                image_path = received_imgs_basename % received_imgs_counter
                print("image saved ", image_path)

                # gif = tk.PhotoImage(file=gifpath)
                global received_imgs_dict
                global hiden_msg_counter

                received_imgs_dict[hiden_msg_counter] = image_path
                #  join("./images/", image_path)
                # received_imgs_dict[image_path] = tk.PhotoImage(file=join("./images/", image_path))
                msg_list.insert(tk.END, rec_image_sender+"HIDDEN MESSAGE ID %s" % hiden_msg_counter)
                received_imgs_counter += 1
                hiden_msg_counter += 1

            else:
                print("gone in else ", msg.decode("utf8"))
                msg_list.insert(tk.END, msg.decode("utf8"))
        except OSError:
            break


def send(event=None):
    msg = my_msg.get()
    if len(msg)== 0:
        return
    my_msg.set("")  # Clears input field.
    client_socket.send(msg.encode("utf-8"))


def sendImage(event=None):
    my_msg.set("")
    client_socket.send(bytes_of_images)


def SteganoSend(event=None):
    msg = my_msg.get()
    if len(msg)== 0:
        return
    my_msg.set("")  # Clears input field.
    all_images = [f for f in listdir("./images") if isfile(join("./images", f))]
    stegano_image = random.choice(all_images)

    in_images = "./images/"+stegano_image
    print("++++++++ selected image: %s" % in_images)
    out_images = "./stegano/"+stegano_image

    print("now encoding the image")
    Steganography.encode(in_images, out_images, msg)
    print("Encoding done!")

    test_image = open(out_images, 'rb')
    global bytes_of_images
    bytes_of_images = test_image.read()
    size = len(bytes_of_images)
    size_msg = "IMAGE SIZE IS:%s" % size
    client_socket.send(size_msg.encode("utf-8"))
    print("image size sent: %s" % size_msg)


def Back():
    my_msg.set("{quit}")
    send()
    client_socket.close()
    root.quit()


def display_clicked_content(*ignore):
    imgname = msg_list.get(msg_list.curselection()[0]).split(" ")[-1]
    global received_imgs_dict
    if imgname.isdigit() and int(imgname) in received_imgs_dict:
        print(received_imgs_dict[int(imgname)])
        # canvas = tk.Canvas(root, width=300, height=300)
        # # img = ImageTk.PhotoImage(Image.open(received_imgs_dict[int(imgname)]))
        # img = ImageTk.PhotoImage(Image.open(received_imgs_dict[int(imgname)]))
        # canvas.create_image(20, 20, anchor=tk.NW, image=img)
        # canvas.pack()
        hidden_msg = Steganography.decode(received_imgs_dict[int(imgname)])
        print(hidden_msg)
        txt_hidden_msg.set(hidden_msg)

    else:
        print("The message was clearly sent ", imgname)
        txt_hidden_msg.set("")


def HomePage(MY_USERNAME):

    global Home
    global msg_list
    root.withdraw()
    Home = tk.Toplevel()
    Home.title("Simple Chat Application")
    init(Home)
    root.resizable(0, 0)
    global USERNAME
    lbl_home = tk.Label(Home, text="One room chat application!", font=('times new roman', 20)).pack()
    btn_back = tk.Button(Home, text='Log Out', command=Back).pack(pady=20, padx=60)

    messages_frame = tk.Frame(Home)
    scrollbar = tk.Scrollbar(messages_frame)  # To see through previous messages.
    # this will contain the messages.
    msg_list = tk.Listbox(messages_frame, height=30, width=100, yscrollcommand=scrollbar.set)
    scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
    msg_list.pack(side=tk.LEFT, fill=tk.BOTH)
    msg_list.pack()
    msg_list.bind('<ButtonRelease-1>', display_clicked_content)
    messages_frame.pack()

    lbl_hidden_message = tk.Label(Home, textvariable=txt_hidden_msg, font=('times new roman', 15)).pack()

    entry_field = tk.Entry(Home, textvariable=my_msg, font=('times new roman', 15))
    # entry_field = tk.Text()
    # entry_field.bind("<Return>", send)
    entry_field.pack()  # expand=tk.YES)

    send_button = tk.Button(Home, text="Send clear text", fg='green', command=send)
    send_button.pack()

    stegano_send_button = tk.Button(Home, text="Send stegano text", fg='blue', command=SteganoSend)
    stegano_send_button.pack()

    # msg = client_socket.recv(BUFSIZ).decode("utf8")
    # msg_list.insert(END, msg)

    client_socket.send(MY_USERNAME.encode("utf-8"))
    print("name sent ..", MY_USERNAME)

    receive_thread = Thread(target=receive)
    receive_thread.start()

    # Home.protocol("WM_DELETE_WINDOW", Back)


# init(root)
InitConnection()
WelcomePage()
# LoginPage()
root.mainloop()
