
import socket, sys
# from socket import AF_INET, socket, SOCK_STREAM
from struct import *
from datetime import datetime
from steganography.steganography import Steganography


server_ip_addr = "202.30.29.243"
server_port = 12345

received_imgs_counter = 1
received_imgs_basename = "./sniffedImage/image%s.png"


# Convert a string of 6 characters of ethernet address into a dash separated hex string
def eth_addr(a):
    b = "%.2x:%.2x:%.2x:%.2x:%.2x:%.2x" % (ord(a[0]), ord(a[1]), ord(a[2]), ord(a[3]), ord(a[4]), ord(a[5]))
    return b


# create a AF_PACKET type raw socket (thats basically packet level)
# define ETH_P_ALL    0x0003          /* Every packet (be careful!!!) */
try:
    s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(0x0003))
    # s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
    # s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.ntohs(0x0003))
    # ADDR = ("202.30.29.243", 12345)
    # s.connect(ADDR)
except socket.error, msg:
    print 'Socket could not be created. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    sys.exit()

# receive a packet
while True:
    packet = s.recvfrom(65565)  # 65565

    # packet string from tuple
    packet = packet[0]

    # parse ethernet header
    eth_length = 14

    eth_header = packet[:eth_length]
    eth = unpack('!6s6sH', eth_header)
    eth_protocol = socket.ntohs(eth[2])
    # print 'Destination MAC : ' + eth_addr(packet[0:6]) + ' Source MAC : ' + eth_addr(
    #     packet[6:12]) + ' Protocol : ' + str(eth_protocol)

    # Parse IP packets, IP Protocol number = 8
    if eth_protocol == 8:
        # Parse IP header
        # take first 20 characters for the ip header
        ip_header = packet[eth_length:20 + eth_length]

        # now unpack them :)
        iph = unpack('!BBHHHBBH4s4s', ip_header)

        version_ihl = iph[0]
        version = version_ihl >> 4
        ihl = version_ihl & 0xF

        iph_length = ihl * 4

        ttl = iph[5]
        protocol = iph[6]
        s_addr = socket.inet_ntoa(iph[8])
        d_addr = socket.inet_ntoa(iph[9])
        if d_addr != server_ip_addr:
            continue

        # TCP protocol
        if protocol == 6:
            t = iph_length + eth_length
            tcp_header = packet[t:t + 20]

            # now unpack them :)
            tcph = unpack('!HHLLBBHHH', tcp_header)

            source_port = tcph[0]
            dest_port = tcph[1]
            if dest_port != server_port:
                continue

            sequence = tcph[2]
            acknowledgement = tcph[3]
            doff_reserved = tcph[4]
            tcph_length = doff_reserved >> 4

            h_size = eth_length + iph_length + tcph_length * 4
            data_size = len(packet) - h_size

            # get data from the packet
            data = packet[h_size:]

            if len(data) == 0:
                continue

            print(str(datetime.now()))
            # print('Destination MAC : ' + eth_addr(packet[0:6]) + ' Source MAC : ' + eth_addr(
            #     packet[6:12]) + ' Protocol : ' + str(eth_protocol))

            print('IP Version : ' + str(version) + ' IP Header Length : ' + str(ihl) + ' TTL : ' + str(
                ttl) + ' Protocol : ' + str(protocol) + ' Source Address : ' + str(
                s_addr) + ' Destination Address : ' + str(d_addr))

            print('TCP Source Port : ' + str(source_port) + ' Dest Port : ' + str(
                dest_port) + ' Sequence Number : ' + str(
                sequence) + ' Acknowledgement : ' + str(acknowledgement) + ' TCP header length : ' + str(tcph_length))

            if str(data).strip().startswith("\x89PNG"):  # \x89\x50\x4e\x47\x0d\x0a\x1a\x0a
                print("===========")
                image_file = open(received_imgs_basename % received_imgs_counter, "wb")
                image_file.write(data)
                image_file.close()

                hidden_msg = Steganography.decode(received_imgs_basename % received_imgs_counter)
                print("ALERT at %s" % str(datetime.now()))
                print("Stegano message found from: %s" % (str(s_addr) + ":" + str(source_port)))
                print("The hidden message is: %s \n" % hidden_msg)
                received_imgs_counter += 1
            else:
                print("Data : " + data)

            # print("the data string is: ", str(data).strip()[0:5])

            # print('Data : ' + data)

        # UDP packets
        elif protocol == 17:
            u = iph_length + eth_length
            udph_length = 8
            udp_header = packet[u:u + 8]

            # now unpack them :)
            udph = unpack('!HHHH', udp_header)

            source_port = udph[0]
            dest_port = udph[1]
            if dest_port != server_port:
                continue

            length = udph[2]
            checksum = udph[3]

            print 'UDP Source Port : ' + str(source_port) + ' Dest Port : ' + str(dest_port) + ' Length : ' + str(
                length) + ' Checksum : ' + str(checksum)

            h_size = eth_length + iph_length + udph_length
            data_size = len(packet) - h_size

            # get data from the packet
            data = packet[h_size:]

            print 'Data : ' + data

        # some other IP packet like IGMP
        else:
            print 'Protocol other than TCP/UDP/ICMP'

        print
    else:
        continue
        # pass
        # print("The eth_protocol: %s" % eth_protocol)
