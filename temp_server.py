from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread
import sqlite3
import datetime
import time

def accept_incoming_connections():
    """Sets up handling for incoming clients."""
    while True:
        client, client_address = SERVER.accept()
        print("%s:%s has connected." % client_address)
        # client.send(bytes("Greetings from the Us! Now type your name and press enter!", "utf8"))
        addresses[client] = client_address
        Thread(target=handle_client, args=(client,client_address,)).start()


def handle_client(client, ipaddr):  # Takes client socket as argument.
    """Handles a single client connection."""

    registered = False
    name = ''
    while not registered:
        credentials = client.recv(BUFSIZ).decode("utf8")
        service = credentials.split("##")[0]
        name = credentials.split("##")[1]
        passwrd = credentials.split("##")[2]
        if service == 'reg':
            rconn = sqlite3.connect('usersdb.db')
            reg_conn = rconn.cursor()
            reg_conn.execute('''SELECT * FROM users WHERE username=?''', (name,))
            if len(reg_conn.fetchall()) > 0:
                client.send(bytes('500', "utf8"))
            elif len(reg_conn.fetchall()) == 0:
                ip_address = str(ipaddr[0])+":"+str(ipaddr[1])
                # datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                reg_conn.execute('INSERT INTO users values (?,?,?,?,?)', (None, time.time(), name, passwrd, ip_address))
                rconn.commit()
                # client.send(bytes('200', "utf8"))
                # registered = True
                break
            else:
                client.send(bytes('400', "utf8"))
        elif service == 'log':
            lconn = sqlite3.connect('usersdb.db')
            log_conn = lconn.cursor()
            log_conn.execute('SELECT * FROM users WHERE username=? and password=?', (name, passwrd))
            if len(log_conn.fetchall()) > 0:
                registered = True
                break
            else:
                client.send(bytes('400', "utf8"))
        else:
            client.send(bytes("Uknown error!", "utf8"))

    client.send(bytes('200', "utf8"))
    print("waiting for name ...")
    name = client.recv(BUFSIZ).decode("utf8")
    print("received ", name)
    welcome = 'Welcome %s, GLAD TO MEET YOU!' % name
    client.send(bytes(welcome, "utf8"))

    msg = "%s has joined the chat!" % name
    broadcast(bytes(msg, "utf8"))
    clients[client] = name

    while True:
        msg = client.recv(BUFSIZ)
        if msg != bytes("{quit}", "utf8"):
            broadcast(msg, name+": ")
        else:
            client.send(bytes("{quit}", "utf8"))
            client.close()
            del clients[client]
            broadcast(bytes("%s has left the chat." % name, "utf8"))
            break


def broadcast(msg, prefix=""):  # prefix is for name identification.
    """Broadcasts a message to all the clients."""

    for sock in clients:
        sock.send(bytes(prefix, "utf8")+msg)


clients = {}
addresses = {}

HOST = ''
PORT = 4321
BUFSIZ = 1024
ADDR = (HOST, PORT)

SERVER = socket(AF_INET, SOCK_STREAM)
SERVER.bind(ADDR)

conn = sqlite3.connect('usersdb.db')
c = conn.cursor()
# Create table
c.execute('''CREATE TABLE IF NOT EXISTS users
             (id INTEGER PRIMARY KEY AUTOINCREMENT, date TIMESTAMP CURRENT_TIMESTAMP, 
             username text, password CHAR(56), addr text)''')
# Save (commit) the changes
conn.commit()

if __name__ == "__main__":
    SERVER.listen(5)
    print("Waiting for connection...")
    ACCEPT_THREAD = Thread(target=accept_incoming_connections)
    ACCEPT_THREAD.start()
    ACCEPT_THREAD.join()
    conn.close()
    SERVER.close()
