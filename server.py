from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread
import sqlite3
import datetime
import time

exchanged_imgs_counter = 1
exchanged_imgs_basename = "serverImages/image%s.png"



def accept_incoming_connections():
    """Sets up handling for incoming clients."""
    while True:
        client, client_address = SERVER.accept()
        print("%s:%s has connected." % client_address)
        # client.send(bytes("Greetings from the Us! Now type your name and press enter!", "utf8"))
        addresses[client] = client_address
        Thread(target=handle_client, args=(client, client_address,)).start()


def handle_client(client, ipaddr):  # Takes client socket as argument.
    """Handles a single client connection."""

    registered = False
    name = ''
    while not registered:
        credentials = client.recv(BUFSIZ).decode("utf8")
        service = credentials.split("##")[0]
        name = credentials.split("##")[1]
        passwrd = credentials.split("##")[2]
        if service == 'reg':
            rconn = sqlite3.connect('usersdb.db')
            reg_conn = rconn.cursor()
            reg_conn.execute('''SELECT * FROM users WHERE username=?''', (name,))
            if len(reg_conn.fetchall()) > 0:
                client.send(bytes('500', "utf8"))
            elif len(reg_conn.fetchall()) == 0:
                ip_address = str(ipaddr[0])+":"+str(ipaddr[1])
                # datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                reg_conn.execute('INSERT INTO users values (?,?,?,?,?)', (None, time.time(), name, passwrd, ip_address))
                rconn.commit()
                # client.send(bytes('200', "utf8"))
                # registered = True
                break
            else:
                client.send(bytes('400', "utf8"))
        elif service == 'log':
            lconn = sqlite3.connect('usersdb.db')
            log_conn = lconn.cursor()
            log_conn.execute('SELECT * FROM users WHERE username=? and password=?', (name, passwrd))
            if len(log_conn.fetchall()) > 0:
                registered = True
                break
            else:
                client.send(bytes('400', "utf8"))
        else:
            client.send(bytes("Uknown error!", "utf8"))

    client.send(bytes('200', "utf8"))
    print("waiting for name ...")
    name = client.recv(BUFSIZ).decode("utf8")
    print("received ", name)
    welcome = 'Welcome %s! Here You can chat in clear text or using hidden message. ' \
              'In case of a hidden message, just click on it!' % name
    client.send(bytes(welcome, "utf8"))

    msg = "%s has joined the chat!" % name
    broadcast(bytes(msg, "utf8"))
    clients[client] = name

    while True:
        msg = client.recv(BUFSIZ)
        global image_data
        if msg == bytes("{quit}", "utf8"):
            client.send(bytes("{quit}", "utf8"))
            client.close()
            del clients[client]
            broadcast(bytes("%s has left the chat." % name, "utf8"))
            break
        elif bytes("IMAGE SIZE IS", "utf8") in msg:
            image_size = msg.decode().split(':')[-1]
            got_image_size_mgs = 'SIZE OF IMAGE RECEIVED:' + str(image_size)
            client.send(bytes(got_image_size_mgs.strip(), "utf8"))
            print(msg, got_image_size_mgs)
            image_data = client.recv(int(image_size))
            broadcastImage(image_data, name.upper() + ": ", image_size)

            # global exchanged_imgs_counter
            # global exchanged_imgs_basename
            # print(exchanged_imgs_counter)
            # image_file = open(exchanged_imgs_basename % exchanged_imgs_counter, "wb")
            # exchanged_imgs_counter += 1
            # image_file.write(image_data)
            # image_file.close()
        elif bytes('SIZE OF IMAGE RECEIVED', "utf8") in msg:
            # print("received ack now rebroad to %s" % name.upper(), msg)
            client.send(image_data)
            print("image sent to: %s with size %s" % (clients[client], str(len(image_data))))
        else:
            print("fallen in else: from %s " % name, msg[0:20])
            broadcast(msg, name.upper() + ": ")


def broadcast(msg, prefix=""):  # prefix is for name identification.
    """Broadcasts a message to all the clients."""
    for sock in clients:
        sock.send(bytes(prefix, "utf8")+msg)


def broadcastImage(this_image_bytes, sender_name, size_of_the_image=0):  # prefix is for name identification.
    """Broadcasts a message to all the clients."""
    print("\nin broadcast images ")
    for sock in clients:
        print("handling client: %s" % clients[sock])
        size_msg = "IMAGE SIZE IS#%s#%s" % (size_of_the_image, sender_name)
        sock.send(size_msg.encode("utf-8"))
        print("size sent to %s as of: %s" % (clients[sock], str(size_of_the_image)))
        # sock.send(this_image_bytes)
        # continue

        # response_msg = sock.recv(BUFSIZ).decode("utf8")
        # print("response after size sent to %s:" % clients[sock], response_msg)
        # if 'SIZE OF IMAGE RECEIVED' in response_msg:
        #     print("received ack now rebroad to %s" % clients[sock], response_msg)
        #     sock.send(this_image_bytes)
        #     print("image sent to: %s" % clients[sock])
        # else:
        #     print("else received msg: ", response_msg)


clients = {}
addresses = {}

HOST = ''
PORT = 12345
BUFSIZ = 256
ADDR = (HOST, PORT)

image_data = 0

SERVER = socket(AF_INET, SOCK_STREAM)
SERVER.bind(ADDR)

conn = sqlite3.connect('usersdb.db')
c = conn.cursor()
# Create table
c.execute('''CREATE TABLE IF NOT EXISTS users
             (id INTEGER PRIMARY KEY AUTOINCREMENT, date TIMESTAMP CURRENT_TIMESTAMP, 
             username text, password CHAR(56), addr text)''')
# Save (commit) the changes
conn.commit()

if __name__ == "__main__":
    SERVER.listen(5)
    print("Waiting for connection...")
    ACCEPT_THREAD = Thread(target=accept_incoming_connections)
    ACCEPT_THREAD.start()
    ACCEPT_THREAD.join()
    conn.close()
    SERVER.close()
